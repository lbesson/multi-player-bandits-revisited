% -----------------------------------------------------------------
% -----------------------------------------------------------------
\section{Discussions on regret lower bounds}
\label{app:discussions_lowerbounds}

This sections gives a quick overview of the state-of-the-art lower-bounds for regret for both single player and centralized multi-player, and a discussion on some hypothesis required by our lower-bound.


% -----------------------------------------------------------------
\subsection{Regret lower bound for single player bandit}
\label{app:SP_lowerbound}

For only one player ($M = 1$), centralized and decentralized policies are the same.
The well-known result\footnote{See Section~2.3 by \cite{Bubeck12} for a more recent and easier proof. Note that $1\text{-}\mathrm{worst}=\{1,\dots,K\}\setminus\{1^*\}$.} from \cite{LaiRobbins85} is the following.

\begin{theorem}[\citeauthor{LaiRobbins85}'s lower-bound]\label{thm:LaiRobbins85}
  For any \emph{uniformly efficient} policy $\rho$ for the single-player problem,
  the regret $R_T(\mu, 1, \rho)$ satisfies,
  for any problem $\mu \in \Lambda^K$, with non-zero gap $\mu_{1}^* - \mu_{2}^*$:
  \begin{equation}\label{eq:LaiRobbins85}
    \mathop{\lim\inf}\limits_{T \to +\infty} \frac{R_T(\mu, 1, \rho)}{\log(T)}
      \geq \sum_{k \in 1\text{-}\mathrm{worst}} \frac{(\mu_{1}^* -  \mu_k)}{\kl(\mu_k, \mu_{1}^*)}.
  \end{equation}
\end{theorem}

Note that our multi-player bound in Theorem~\ref{thm:BetterLowerBound} is exactly this result, if $M = 1$.
The value of $C(\mu, 1)$, \ie, the constant for $1$ player, is
the same as the value from the \citeauthor{LaiRobbins85}'s lower-bound.

For $M = K$, the constant $C(\mu, M)$ is $0$ as it is a sum on
\Mworst{} which is an empty set. The lower bound is less informative
in this case, and indeed the only cause of regret are the collisions
$\Ccal_k(T)$, not the choice of worst arms.
%
% So far, we cannot say more about this observation.


% -----------------------------------------------------------------
\subsection{Regret lower bound for centralized multi-player bandit}
\label{app:centralizedMP_lowerbound}

If $M > 1$ but a centralized agent can coordinate the players,
\ie, in the multiple-play Multi-Armed Bandit setting,
\cite{Anantharam87a} extended the previous result and proved the following theorem.

\begin{theorem}[\citeauthor{Anantharam87a}'s lower-bound]\label{thm:Anantharam87a}
  For any \emph{uniformly efficient} (centralized or not) policy $\rho$,
  the regret $R_T(\mu, M, \rho)$ satisfies,
  for any problem $\mu \in \Lambda^K$, with non-zero gap $\mu_M^* - \mu_{M+1}^*$:
  \begin{equation}\label{eq:Anantharam87a}
  \mathop{\lim\inf}\limits_{T \to +\infty} \frac{R_T(\mu, M, \rho)}{\log(T)}
  \geq \sum_{k \in \Mworst} \frac{(\mu_M^* -  \mu_k)}{\kl(\mu_k, \mu_M^*)} .
  \end{equation}
\end{theorem}


% -----------------------------------------------------------------
\subsection{Discussion on weighted fairness}
\label{app:weightedFairness}

% \todo[inline]{FIXME detail how to choose the $(a_{j,k})$, or prove that they exist?}
For simplicity, and because it seems to be the more plausible setting,
in Section~\ref{sec:lowerbound} we choose $a_{j,k} = 1/M$,
but our Theorem~\ref{thm:BetterLowerBound}
is valid in a more general setting, for any $(a_{j,k})_{j,k}$, as long as
$\min_{j,k} a_{j,k} > 0$ and the matrix $(a_{j,k})_{j,k}$ is \emph{doubly stochastic}
(\ie, lines and columns all sum to $1$).
%
Having one value $a_{j,k}$ smaller or larger than $1/M$ means that the distributed policy favors or disfavors a player to access a particular arm, but all players run the same algorithms indistinguishably, and up-to a permutation of the arms, their order is not important. The intuition of a fair policy, meaning all of the $M$ players have, in average, the same chance of accessing each of the $M$ best arms, is only valid for uniform weights $a_{j,k} = 1/M$.

% \todo[inline]{FIXME we need to justify that there exists a policy $\rho$ satisfying these two properties...}
% \todo[inline]{FIXME The hard part is the fairness with weights $a_{j,k}$, the uniform efficiency is a corollary of the upper-bound.}