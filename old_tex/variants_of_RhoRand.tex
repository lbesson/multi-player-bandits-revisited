% -----------------------------------------------------------------
% -----------------------------------------------------------------
\section{Variants of the \rhoRand{} orthogonalization algorithm}
\label{app:variantsRhoRand}

\todo[inline]{FIXME remove this appendix ? I want to keep it, but too long appendix is not nice...}

Let us describe some possible variants on the \rhoRand{} orthogonalization algorithm.
%
% We implemented all of them,
% \todo[inline]{FIXME change this, either by including experiments here or removing this remark !}
Extensive experiments were also conducted on these variants,
and none of them performs uniformly better that \rhoRand,
but due to space constraint,
results concerning these variants could not be presented in Section~\ref{sec:experiments}.
%
Additionally, theoretical analysis of these variants is harder,
so we preferred to focus our analysis on \rhoRand.


% -----------------------------------------------------------------
\subsection{Intuition about \rhoRand}
\label{app:intuition_rhoRand}

The idea behind this strategy is to use the index policy $P$
to collect statistics about the streams $Y_{k,s}$ in all the $K$ arms,
as in the single-player or the centralized cases,
but to not always aim at the optimistic arm, as this greedy behavior would result in only collisions if implemented by all $M$ players.
%
Instead, the players are assumed to know $M$, and as their goal is to orthogonally select $M$ different arms from \Mbest, they have to converge to a situation where they all know
which of the $K$ arms are the $M$ best (\ie, the set \Mbest), and where they all select a different arm from \Mbest.
%
The identification of the set \Mbest{} is done by the policy $P$,
that should be a sequential single-player decision making policy as efficient as possible,
and the orthogonal selection is done by randomly changing ranks as long as a collision is experienced.
Hopefully, the process converges to an orthogonal configuration, or at least stays in an orthogonal configuration as long as possible, and so fewer collisions are experienced if all the $M$ players correctly identified \Mbest.


% -----------------------------------------------------------------
\subsection{The \rhoFixedRank{} variant}
\label{app:rhoFixedRank}

When giving the value of $M$ to all the players, assumed to be fixed in time and known by the Base Station, in the beginning of the initialization phase of the communication game,
the BTS could easily also give a fixed rank to each players,
to directly have an orthogonal configuration.
The \rhoFixedRank{} algorithm works exactly like \rhoRand{} described in Algorithm~\ref{algo:rhoRand}, except that player $j\in\{1,\dots,M\}$
starts with a rank $q^j = j + 1$, \emph{fixed in time}.
%
It is important to notice that this variants \rhoFixedRank{} does \emph{not} achieve zero collision, as the index policy $P$ of each player requires a learning time to have a correct estimate of the set \Mbest{} and the ranking of the $M$ best arms.
%
In fact, \rhoFixedRank{} obtains terrible performances, and has linear regret on all the simulated scenarios.
%  as illustrated in Figure~\ref{fig:XXX}.
% simply because the indexes of used policies, \UCB{} or \klUCB, are mainly designed to tackle efficiently the exploration-exploitation trade-off when arms with highest index are selected, not
% \todo[inline]{FIXME explain better why it should have bad performance...}

% to be used with ranks equals to $1$.
% Except for player $j=0$, the other players use a fixed rank and
% imposing it to be no

% \todo[inline]{include good figure reference}


% -----------------------------------------------------------------
\subsection{The \rhoRandRot{} variant}
\label{app:rhoRandRot}

This variants tries to ensure a perfectly fair network usage for the players,
not on average on an infinite number of games (like it is assumed by hypothesis, see Definition~\ref{def:FairPolicy}),
but on every games.
%
Indeed, when the $M$ best arms have been identified and correctly ranked by the $M$ players, one of them will be lucky and almost always aim at the best arm in the remaining time steps.
And players have no way to know when this starts to happen, as they cannot communicate with each other.

One way to try to make \rhoRand{} perfectly fair would be to make them use a rotating rank, from the beginning (to ensure that after having learn the correct ranking of the $M$ best arms the players share in time the best arms).
player $j$ will aim at the $1 + ((q^j(t) + t) \;\mathrm{mod}\; M)$ best arm,
instead of the $q^j(t)$.
However, this idea also yields terrible performances,
% as illustrated in Figure~\ref{fig:XXX},
because the exploration process
of the index policy is much more perturbed by \rhoRandRot{} than by \rhoRand.
% \todo[inline]{include good figure reference. And explain better why \rhoRandRot{} should perform worse than \rhoRand.}


% -----------------------------------------------------------------
\subsection{The \rhoRandSticky{} variant}
\label{app:rhoRandSticky}

\cite{Rosenski16} proposed the \emph{Musical Chair} policy for the same learning problem, in the case where $M$ is unknown to the player,
based on the idea of explore-then-commit, that relies on decoupling exploration and exploitation.
The $M$ players will be given a ``large enough'' time $T_0$,
the duration of a first exploration-only phase, during which
they all access uniformly every arm, to estimate their mean availabilities and count collisions.
If they all implement the same uniform access, then the number of collisions after $T_0$ is used to compute an estimate on $M$.
After this first phase, they focus on what they believe is the \Mbest{} arms,
and simply play a ``musical chair'' phase: as long as player $j$ encounters collisions, it changes its arm.
This second phase converges in finite time, to an orthogonal and optimal configuration, given that the players all have a correct estimate of both $M$ and \Mbest.
%
% \todo[inline]{Hard criticism, remove.}
Even if the authors claimed that this policy yields a \emph{finite regret} (with high probability), numerically it performs poorly in terms of expected regret when $T_0$ is not finely tuned by knowing $M$, the problem $\mathbf{\mu}$ and the horizon $T$.
The only theorem given by \cite{Rosenski16} for choosing a ``large enough'' value for $T_0$
depends on all the (unknown) parameters and suggest completely unrealistic values\footnote{See \url{banditslilian.gforge.inria.fr/docs/Policies.MusicalChair.html} for examples of computation of ``large enough'' $T_0$ computed from their theoretical analysis.},
leading to finite but very large regret.

But the Musical Chair policy gives an interesting idea: once a player has chosen an arm, the number of collisions could be reduced if it never changes its decision.
We propose a variant of \rhoRand{} using this idea, called \rhoRandSticky,
with a parameter $T_1 \in \mathbb{N}\cup\{\infty\}$.
Instead of always selecting a new rank after a collision,
if the player has been using the same rank for more than $T_1$ steps without having collisions, then it will never change ranks again\footnote{Note that this definitive decision is done on ranks, not arms.}.
Depending on the value of $T_1$, the \rhoRandSticky{} policy can
simply be \rhoRand{}, with $T_1=\infty$,
or a naively selfish policy with $T_1=0$ (where each player plays with rank $1$),
and for any value in between, $1 \leq T_1 < \infty$, the smaller $T_1$ the faster \rhoRandSticky{} is to converge to a fixed configuration, in terms of ranks.
%
This variant has an obvious weakness of depending on one more parameter, $T_1$, for which we have no intuition nor theoretical analysis
helping to tune it for a specific problem.
In other words, \rhoRandSticky{} can always perform at least as well as \rhoRand, for a family of problems or a particular problem, for a certain value of the parameter $T_1$,
but we have not yet found any heuristic or result to guide the choice of its value.
%
Empirically, on a fixed problem, when we compare players using several versions of \rhoRandSticky, for different $T_1$, usually only one is efficient, but no clear relationship appeared between the problem parameters, $K,M,\mathbf{\mu}$, and the ``best'' value for $T_1$.


% -----------------------------------------------------------------
\subsection{The \rhoRandALOHA{} variant}
\label{app:rhoRandALOHA}

\cite{Avner15} proposed the \emph{MEGA} policy for the same learning problem,
based on combining two probabilistic policies: an $\varepsilon$-greedy algorithm for learning the arms means, and a probability pursuit policy for the orthogonalization process, identical for every player.
%
The $\varepsilon$-greedy policy uses a fixed learning rate $\varepsilon_t=\varepsilon\in(0,1)$, or a non-increasing sequence $(\varepsilon_t)_{t\in\mathbb{N}}\in(0,1)$, and at every time step, with probability $\varepsilon_t$ the player explores an arm, picked uniformly at random, and with probability $1-\varepsilon_t$ the player exploits the arm with highest empirical mean.
%
The probabilistic orthogonalization process uses a probability $p(t)\in(0,1)$ of staying on the same arm in case of collision.
It uses an initial value $p_0\in(0,1)$, and a rate $\alpha$, and $p(t)$ follows this equation: if no collision at time $t$, $p(t+1) = \alpha p(t) + (1-\alpha)$, and in case of collision, with probability $p(t)$ it stays on the same arm, with probability $1-p(t)$ it moves to a different arm, and reinitialize $p(t+1) = p_0$.
%
\cite{Avner15} proposed another refinement: first mark each arm as ``available'', and then after a collision mark the colliding arm for ``unavailable'' for some time. The maximum unavailability time, $\delta_{\text{next}}(t)$ is proposed to be taken $\log(t)$ or $t^{\beta}$ (for $\beta\in(0,1)$), and the unavailability is randomly sampled from $U(1,\dots,\delta_{\text{next}}(t))$.
%
Although being able to perform very well on some problems, this MEGA policy
requires a fine tuning, of both the parameters $p_0, \alpha, \eta_t$ and the unavailability function ($\delta_{\text{next}}(t)=\log(t)$ or $t^{\beta}$),
in order to be efficient.
Except on the same problem as the one used and details by \cite{Avner15}, with $M=K=2$ arms,
we have not been able to find, for any other simple problem,
a ``good'' set of parameters that would give MEGA reasonable performances.
%
Their proposal had another missing detail: what should the player do if all arms are tagged as unavailable? In the considered model, a player should communicate at every time step, otherwise the regret is defined differently.
Concretely, giving a reward of $0$ to a player who chose to not transmit solve the problem but it is not perfectly satisfying.
% \todo[inline]{Should we need to explain this more in details?}

But the MEGA policy gives two interesting ideas to reduce collisions:
players could memorize the colliding arms or ranks, and tag them as unavailable for a (fixed or not) certain time,
and players could have a lower chance of changing ranks after a collision if the used with success the same rank for some time.
%
We propose a variant of \rhoRand{} using these ideas, called \rhoRandALOHA,
with parameters $p_0,\alpha\in(0,1)$ and a time-threshold function for unavailability.
For a player $j$, instead of always selecting a new rank after a collision,
keep rank with probability $p^j(t)$ (in which case $p^j(t+1) = \alpha p^j(t) + (1-\alpha)$),
and change for a new rank taken uniformly at random with probability $1-p^j(t)$, \ie, $q^j(t+1) \sim U(\{1,\dots,M\}\setminus\{q^j(t)\})$ (in which case $p^j(t+1) = p_0$).
%
After a collision, a rank can also be tagged as unavailable, for one or more time steps, depending on the threshold function.
Depending on the value of the three parameters, the \rhoRandALOHA{} policy can
be \rhoRand{}, for instance, with $\alpha=1$, $p_0=1/M$ and threshold function $\delta_{\text{next}}(t)=1$,
or variants with different behaviors.
%
This variant also has an obvious weakness of depending on not one but three parameters, $\alpha,p_0$ in $(0,1)$ and a function $\delta_{\text{next}}(t)$, for which we have no intuition nor theoretical analysis
helping to tune them for a specific problem.
In other words, \rhoRandALOHA{} can also always perform at least as well as \rhoRand, for a family of problems or a particular problem, for a certain value of its parameters,
but we have not find yet any heuristic or result to guide the choice of its value, and nothing was neither intuited nor proved by \cite{Avner15}.
%
Empirically, even on a fixed problem, we found it impossible to tune the three parameters in order for a \rhoRandALOHA{} policy to have satisfying performances.
%
% \todo[inline]{FIXME all this does not like the ALOHA \cite{Abramson70,Roberts75} protocol... at all... So, rename it to \rhoRand{} MEGA or ??}


% -----------------------------------------------------------------
\subsection{The \rhoRandEst{} variant}
\label{app:rhoRandEst}

This last variant is tackling a slightly different problem.
%
We pointed out that the hypothesis of knowing the fixed-in-time value of $M$ can seem unrealistic physically,
and one way to eliminate this consideration would be to \emph{learn}
the number of players as well.
%
The \rhoRandEst{} policy was proposed by \cite{Anandkumar11} for this task,
and uses an estimated $\widehat{M}(t)$ for $M$.
Essentially it starts with assuming an $\widehat{M}(0)=1$, for each player,
and let $\widehat{M}(t)$ grows one by one, in order to correctly estimate $M$ and hopefully not overestimate nor underestimate it.
Every time a collision is observed, \rhoRandEst{} tries to know if the total number of collisions observed so far is more probable if $\widehat{M}(t)$ or $\widehat{M}(t)+1$ players were present from start. If $\widehat{M}(t)+1$ is more probable, then $\widehat{M}(t+1) := \widehat{M}(t)+1$.
%
This is explained in details by \cite{Anandkumar11}, Section~V,
and the announced regret bound is $R_T = \bigO{f(T) \log(T)}$
for any function $f$ such that $f(T) = \omega(1)$ (\ie, $1 = \smallO{f(T)}$) and $f(T)\to\infty$ for $T\to\infty$.
The proof was rather vague, and we preferred to focus on decentralized learning
with the two hypotheses of static players configuration and known number of players.
%
Empirically, it performs almost as well as \rhoRand, with the choice $f(T)=T$, even if the guaranteed regret is far worse ($\bigO{T \log(T)}$).


% -----------------------------------------------------------------
\subsection{Comparison}
\label{app:comparison_rhoRand}

This section presented possible extensions or variants of the \rhoRand{} policy,
some of them trying to improve it by fixing one of its weakness (absence of fairness, naive selection of ranks, etc), others by target a harder problem (\eg, if $M$ is unknown).
%
Extending the theoretical results obtained in Section~\ref{sec:upperbounds} to these variants is not easy, and for the \rhoRandEst{} policy it was attempted by \cite{Anandkumar11}.
Empirically, all the variants perform uniformly dramatically worse than \rhoRand,
except for \rhoLearn{} and \rhoRandEst, which performs slightly worse.
However, they both tackle a harder problem, where the number of players $M$ is unknown to each player.
