% --------------------------------------------------------------------------------

The following lemma will be used several times,
to prove that \MCTopM{} has a logarithmic number of collisions.

\begin{lemma}[Elementary lemma for \MCTopM-\klUCB]\label{lem:elementaryLemmaMCTopM}
    For any $\boldsymbol{\mu}\in\cP_M$,
    any player $j \in \{1, \dots, M\}$ using \MCTopM-\klUCB,
    and any two arms $k \neq k' \in \{1, \dots, K\}$,
    let $\phi_{k,k'}^j(t)$ be the event
    $
        \overline{C^j(t-1)}
        \wedge \left(A^j(t-1) = k \in \TopM(t-1)\right)
        \wedge k \notin \TopM(t)
        \wedge C^j(t)
        \wedge \left(A^j(t) = k' \in \TopM(t) \cap \{ m : g_m^j(t-1) \leq g_k^j(t-1) \}\right)
    $
    Then we have
    \begin{equation}\label{eq:elementaryLemmaMCTopM}
        \sum_{t=1}^{T} \Pr(\phi_{k,k'}^j(t)) \mathop{=}\limits_{T \to +\infty} \bigO{\log T}
    \end{equation}
\end{lemma}

\begin{proof}\label{proof:elementaryLemmaMCTopM}
    If $k$ or $k' \in \Mworst$, this result directly comes from
    Lemma~\ref{lem:SubOptimalSelections} controlling the selections of worst arms,
    as $\phi_{k,k'}^j(t)$ implies $A^j(t) = k'$ and $A^j(t-1) = k$,
    and so $\sum_{t=1}^T \phi_{k,k'}^j(t) \leq \min(\E[T_k^j(T-1)], \E[T_{k'}^j(T-1)])$,
    and at least one term is $\bigO{\log T}$ from Lemma~\ref{lem:SubOptimalSelections}
    and both are always $\bigO{T}$,
    so $\sum_{t=1}^T \phi_{k,k'}^j(t) = \bigO{\log T}$.

    Now assume that $k, k' \in \Mbest$.
    The event $\phi_{k,k'}^j(t)$ we focus on implies
    $(g_{k'}^j(t-1) \leq g_k^j(t-1)) \wedge (A^j(t-1)=k)$
    and
    $(g_k^j(t) \leq g_{k'}^j(t)) \wedge (A^j(t)=k')$.
    %
    If $\mu_k \geq \mu_{k'}$,
    $\phi_{k,k'}^j(t)$ implies $g_k^j(t) \leq g_{k'}^j(t)$,
    and
    if $\mu_k < \mu_{k'}$, then
    $\phi_{k,k'}^j(t)$ implies $g_{k'}^j(t-1) \leq g_k^j(t-1)$,
    both with $(A^j(t-1)=k) \wedge (A^j(t)=k')$.
    Both cases are dealt with similarly,
    so let focus on the first one.

    Assume $\mu_k \geq \mu_{k'}$. Then,
    \begin{align*}
        \sum_{t=1}^{T} \Pr(\phi_{k,k'}^j(t))
        &\leq
        \sum_{t=1}^{T} \Pr\left((g_k^j(t) \leq g_{k'}^j(t)) \wedge (A^j(t-1)=k) \wedge (A^j(t)=k')\right)
        \intertext{Either $g_{k'}^j(t) > \mu_k$ or $g_{k'}^j(t) \geq \mu_k$, and by introducing the two cases,}
        &\leq
        \sum_{t=1}^{T} \Pr\left((g_k^j(t) \leq g_{k'}^j(t) \leq \mu_k) \wedge (A^j(t-1)=k) \wedge (A^j(t)=k')\right)
        \\
        &+ \sum_{t=1}^{T} \Pr\left((g_k^j(t) \leq g_{k'}^j(t)) \wedge (g_k^j(t-1) < \mu_k) \wedge (A^j(t-1)=k) \wedge (A^j(t)=k')\right)
        \intertext{}
    \end{align*}

    \todo[inline]{FIXME finish this, I have all the pieces but need to write}

    % \qedhere  % XXX if the QED symbol should be above (for item or $$...$$)
\end{proof}


We prove now the Lemma~\ref{lem:collisionsRandTopM}.

\todo[inline]{FIXME finish this proof}

For player $j$ and time $t$, we introduce the following events
$s^j(t)$, $I_1^j(t),I_2^j(t),I_3^j(t)$ and $\widetilde{C^j}(t)$:
\begin{itemize}
    \item
    $s^j(t)$ denotes when player $j$ is ``fixed on his chair'' at time $t$.
    Formally, $s^j(1)$ is false, and $s^j(t+1)$ is defined inductively from $s^j(t)$ and $A^j(t), C^j(t), \TopM(t)$,
    following the algorithm of \MCTopM:
    \begin{equation}
        s^j(t+1) =
        \left( s^j(t) \vee \left( \overline{s^j(t)} \wedge \overline{C^j(t)} \right) \right)
        \wedge \left( A^j(t) \in \TopM(t+1) \right).
    \end{equation}

    \item
    At step $t$, player $j$ can have three behavior when executing \MCTopM:
    he does not consider changing its arm $(1)$,
    or he changed its arm by sampling $A^j(t)$ uniformly
    from $\TopM(t) \cap \{ m : g_m^j(t-1) \leq g_k^j(t-1) \}$
    $(2)$,
    or he changed its arm by sampling $A^j(t)$ uniformly
    from $\TopM(t)$
    $(3)$.
    The case $(2)$ happens when there is no collision at time $t-1$ but a collision at time $t$,
    and case $(3)$ happens conversely when the player was not fixed on its arm at time $t-1$ (\ie, $\overline{s^j}(t-1)$)
    and experienced a collision at time $t-1$
    (the uniform sampling from $\TopM(t)$ is this ``Musical Chair'' case that gave its name to \MCTopM).
    Formally, this is denoted as
    \begin{align}
        I_1^j(t) &:= \left(A^j(t-1) \in \TopM(t-1)\right) \wedge \left(\overline{C^j(t-1)} \vee s^j(t-1) \right), \\
        I_2^j(t) &:= \left(A^j(t-1) \notin \TopM(t-1)\right) \wedge \overline{C^j(t-1)}, \\
        I_3^j(t) &:= C^j(t-1) \wedge \overline{s^j(t-1)}.
    \end{align}
    The three events $I_1^j(t)$, $I_2^j(t)$ and $I_3^j(t)$ are disjoint and form a partition, for every $j$ and $t$.

    \item
    Finally, introduce $\widetilde{C^j}(t)$ as the collision indicator for player $j$ if he is not yet fixed on its arm,
    that is
    \begin{equation}
        \widetilde{C^j}(t) := C^j(t) \wedge \overline{s^j(t)}.
    \end{equation}
\end{itemize}

First observe that $\sum_{j=1}^M \Pr(C^j(t)) \leq M \sum_{j=1}^M \Pr(\widetilde{C^j}(t))$
as a collision necessarily involves a player not yet fixed on its arm.
We lose precision by focusing on $\Pr(\widetilde{C^j}(t))$ but it eases the proof.
So $\E[\cC_k(T)] \leq M \sum_{j=1}^M \sum_{t=1}^T \Pr(\widetilde{C^j}(t) \wedge (A^j(t) = k))$.
By definition, $\widetilde{C^j}(t)$ implies $\overline{I_1^j(t-1)}$
and so implies $I_2^j(t-1) \wedge I_3^j(t-1)$ (disjoint union)
at times $t-1$.
So $\Pr(\widetilde{C^j}(t) \wedge (A^j(t) = k)) \leq
\Pr(C^j(t) \wedge (A^j(t) = k) \wedge I_2^j(t-1))
+ \Pr(C^j(t) \wedge (A^j(t) = k) \wedge I_3^j(t-1))$.
For the two events:
\begin{itemize}
    \item
    The first probability is bounded by $\Pr(\phi_{k,k'}^j(t))$ from Lemma~\ref{lem:elementaryLemmaMCTopM}, and so the sum for $j$ and $t=1,\dots,T$ is a $\bigO{\log T}$.
    \item
    The second event is $C^j(t) \wedge (A^j(t) = k) \wedge C^j(t-1) \wedge \overline{s^j(t-1)}$ and is denoted $\psi^j(t)$.
    Its number of occurrences will also be bounded as $\bigO{\log T}$,
    by controlling the average length of a sequence of $\wedge_{s=t}^{s=t'} \psi(s)$ (that is $t' - t + 1$) as a constant w.r.t. $T$, \ie, $\bigO{1}$,
    and by controlling the number of such sequences by $\bigO{\log T}$.
\end{itemize}

