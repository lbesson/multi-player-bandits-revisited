# Julia code for our article

## List of programs
### `AlgosMP.jl`
Algorithms for the general multi-players problem

### `mainMultiPlayers.jl`
Launch experiments based on these algorithms.

```bash
julia -p X mainMultiPlayers.jl
```
with `X` = your number of cores will speed them up

### `CriticalCase.jl`
In the particular case K=M, some algorithms are reimplemented with a display option, so that we really see what the algorithm are doing.

----

## Authors
[Emilie Kaufmann](http://chercheurs.lille.inria.fr/ekaufman/research.html), August 2017.
