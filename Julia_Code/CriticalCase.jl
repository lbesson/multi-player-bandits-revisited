#!/usr/bin/env julia

function randmax(vector, rank = 1)
    vector = vec(vector)
    Sorted = sort(vector, rev = true)
    m = Sorted[rank]
    Ind = find(vector .== m)
    index = Ind[floor(Int, length(Ind) * rand()) + 1]
    return (index)
end

function kl(p, q)
    if (p == 0) & (q != 1)
        return log(1 / (1 - q))
    else
        if (p <= 0) p = eps() end
        if (q >= 1) q = 1 - eps() end
        return p * log(p / q) + (1 - p) * log((1 - p) / (1 - q))
    end
end

function klIC(p, d)
    # computes kl confidence interval using binary search
    if (p == 0)
        return 1 - exp( - d)
    elseif (p == 1 / 2)
        return (1 + sqrt(1 - exp( - 2*d))) / 2
    else
        lM = p
        uM = min(1, p + sqrt(d / 2))
        for j = 1:16
            qM = (uM + lM) / 2
            if kl(p, qM) > d
                uM= qM
            else
                lM = qM
            end
        end
        qM = uM
        return qM
    end
end


function DrawTable(mu, T)
# draw a table of rewards
    K = length(mu)
    Table = zeros(K, T)
    for k = 1:K
        ind = find(rand(T) .< mu[k] * ones(T))
        Table[k, ind] = 1
    end
    return(Table)
end


function RunSelfishUCB(Table)
    # Selfish UCB with display
    (K, T) = size(Table)
    S1 = zeros(Int, K)
    N1 = zeros(Int, K)
    U1 = Inf * ones(K)
    S2 = zeros(Int, K)
    N2 = zeros(Int, K)
    U2 = Inf * ones(K)
    NoCollision = 0

    # initialization
    print("\n\nSTART \n\n")
    for t = 1:T
    I1 = randmax(U1)
    I2 = randmax(U2)
    print("Player 1 chooses $(I1)\n")
    print("Player 2 chooses $(I2)\n\n")
    N1[I1] += 1
    N2[I2] += 1
    if (I1 != I2)
        NoCollision += 1
        S1[I1] += Table[I1, t]
        S2[I2] += Table[I2, t]
    else
        print("Collision!\n\n")
    end
    # updating the confidence bounds
    for k in 1:K
        if (N1[k]>0)
            U1[k] = S1[k] / N1[k] + sqrt(log(t) / (2 * N1[k]))
        end
    end
    for k in 1:K
        if (N2[k]>0)
            U2[k] = S2[k] / N2[k] + sqrt(log(t) / (2 * N2[k]))
        end
    end
    print("Confidence bounds for player 1: $(U1)\n")
    print("Confidence bounds for player 2: $(U2)\n")
    print("S1=$(S1) and N1=$(N1)\n")
    print("S2=$(S2) and N2=$(N2)\n")
    end
    print("Total number of collision: $(T- NoCollision) out of $(T)\n")
end


function IterateSelfishUCB(Table)
    # selfish UCB without display
    (K, T) = size(Table)
    S1 = zeros(Int, K)
    N1 = zeros(Int, K)
    U1 = Inf * ones(K)
    S2 = zeros(Int, K)
    N2 = zeros(Int, K)
    U2 = Inf * ones(K)
    NoCollision = 0

    # initialization
    for t = 1:T
        I1 = randmax(U1)
        I2 = randmax(U2)
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
            NoCollision += 1
            S1[I1] += Table[I1, t]
            S2[I2] += Table[I2, t]
        end
        # updating the confidence bounds
        for k in 1:K
            if (N1[k]>0)
                U1[k] = S1[k] / N1[k] + sqrt(log(t) / (2 * N1[k]))
            end
        end
        for k in 1:K
            if (N2[k]>0)
                U2[k] = S2[k] / N2[k] + sqrt(log(t) / (2 * N2[k]))
            end
        end
    end
    return (T- NoCollision)
end


function RunSelfishKLUCB(Table)
    # selfish KLUCB with display
    (K, T) = size(Table)
    S1 = zeros(Int, K)
    N1 = zeros(Int, K)
    U1 = Inf * ones(K)
    S2 = zeros(Int, K)
    N2 = zeros(Int, K)
    U2 = Inf * ones(K)
    LastCollision = 0
    NoCollision = 0

    # initialization
    print("\n\nSTART \n\n")

    for t = 1:T
        I1 = randmax(U1)
        I2 = randmax(U2)
        print("Player 1 chooses $(I1)\n")
        print("Player 2 chooses $(I2)\n\n")
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
            NoCollision += 1
            S1[I1] += Table[I1, t]
            S2[I2] += Table[I2, t]
        else
            print("Collision!\n\n")
            LastCollision = t
        end
        # updating the confidence bounds
        for k in 1:K
            if (N1[k]>0)
                U1[k] = klIC(S1[k] / N1[k], log(t) / N1[k])
            end
        end
        for k in 1:K
            if (N2[k]>0)
                U2[k] = klIC(S2[k] / N2[k], log(t) / N2[k])
            end
        end
        print("Confidence bounds for player 1: $(U1)\n")
        print("Confidence bounds for player 2: $(U2)\n")
        print("S1=$(S1) and N1=$(N1)\n")
        print("S2=$(S2) and N2=$(N2)\n")
    end
    print("Total number of collision: $(T - NoCollision) out of $(T)\n")
    print("The last collision occurend at t=$(LastCollision)\n")
end


function RunSelfishKLUCBInit(Table)
    # selfish KLUCB with a interesting particular initialization, with display
    (K, T) = size(Table)
    S1 = zeros(Int, K)
    N1 = ones(Int, K)
    U1 = 0.5 * ones(K)
    S2 = zeros(Int, K)
    N2 = ones(Int, K)
    U2 = 0.5 * ones(K)
    NoCollision = 0

    # initialization
    print("\n\nSTART \n\n")

    for t = 1:T
        I1 = randmax(U1)
        I2 = randmax(U2)
        print("Player 1 chooses $(I1)\n")
        print("Player 2 chooses $(I2)\n\n")
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
            NoCollision += 1
            S1[I1] += Table[I1, t]
            S2[I2] += Table[I2, t]
        else
            print("Collision!\n\n")
            LastCollision = t
        end
        # updating the confidence bounds
        for k in 1:K
            U1[k] = klIC(S1[k] / N1[k], log(2 + t) / N1[k])
        end
        for k in 1:K
            U2[k] = klIC(S2[k] / N2[k], log(2 + t) / N2[k])
        end
        print("Confidence bounds for player 1: $(U1)\n")
        print("Confidence bounds for player 2: $(U2)\n")
        print("S1=$(S1) and N1=$(N1)\n")
        print("S2=$(S2) and N2=$(N2)\n")
    end
    print("Total number of collision: $(T - NoCollision) out of $(T)\n")
    print("The last collision occurend at t=$(LastCollision)\n")
end

function IterateSelfishKLUCB(Table)
    # selfish KL - UCB without display
    (K, T) = size(Table)
    S1 = zeros(Int, K)
    N1 = zeros(Int, K)
    U1 = Inf * ones(K)
    S2 = zeros(Int, K)
    N2 = zeros(Int, K)
    U2 = Inf * ones(K)
    NoCollision = 0

    for t = 1:T
        I1 = randmax(U1)
        I2 = randmax(U2)
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
        NoCollision += 1
        S1[I1] += Table[I1, t]
        S2[I2] += Table[I2, t]
        end
        # updating the confidence bounds
        for k in 1:K
            if (N1[k]>0)
                U1[k] = klIC(S1[k] / N1[k], log(t) / N1[k])
            end
        end
        for k in 1:K
            if (N2[k]>0)
                U2[k] = klIC(S2[k] / N2[k], log(t) / N2[k])
            end
        end
    end
    return (T - NoCollision)
end


function IterateSelfishKLUCBInit(T)
    # selfish KLUCB with particular init, without display
    S1 = zeros(Int, 2)
    N1 = ones(Int, 2)
    U1 = 0.5 * ones(2)
    S2 = zeros(Int, 2)
    N2 = ones(Int, 2)
    U2 = 0.5 * ones(2)
    NoCollision = 0

    for t = 1:T
        I1 = randmax(U1)
        I2 = randmax(U2)
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
            NoCollision += 1
            S1[I1] += Table[I1, t]
            S2[I2] += Table[I2, t]
        end
        # updating the confidence bounds
        for k in 1:2
            U1[k] = klIC(S1[k] / N1[k], log(2 + t) / N1[k])
        end
        for k in 1:2
            U2[k] = klIC(S2[k] / N2[k], log(2 + t) / N2[k])
        end
    end
    return (T - NoCollision)
end


function RunHybridUCB(Table)
    # HybridUCB (uses ranks on tops of "selfish" indices)
    (K, T) = size(Table)
    S1 = zeros(Int, 2)
    N1 = zeros(Int, 2)
    U1 = Inf * ones(2)
    S2 = zeros(Int, 2)
    N2 = zeros(Int, 2)
    U2 = Inf * ones(2)
    Ranks = rand(1:2, 2)
    NoCollision = 0

    # initialization
    print("\n\nSTART \n\n")

    I1 = rand(1:2)
    I2 = rand(1:2)
    N1[I1] += 1
    N2[I2] += 1
    if (I1 != I2)
        NoCollision += 1
        S1[I1] += Table[I1, 1]
        S2[I2] += Table[I2, 1]
    else
        print("Two initial collisions\n")
    end
    I1 = 3 - I1
    I2 = 3 - I2
    N1[I1] += 1
    N2[I2] += 1
    if (I1 != I2)
        NoCollision += 1
        S1[I1] += Table[I1, 2]
        S2[I2] += Table[I2, 2]
    end
    U1 = S1 ./ N1 + sqrt.(log(2) ./ (2 * N1))
    U2 = S2 ./ N2 + sqrt.(log(2) ./ (2 * N2))
    print("Initial confidence bounds for player 1: $(U1)\n")
    print("Initial confidence bounds for player 2: $(U2)\n")

    for t = 3:T
        I1 = randmax(U1, Ranks[1])
        I2 = randmax(U2, Ranks[2])
        print("Player 1 chooses $(I1)\n")
        print("Player 2 chooses $(I2)\n\n")
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
            NoCollision += 1
            S1[I1] += Table[I1, T]
            S2[I2] += Table[I2, T]
        else
            print("Collision!\n\n")
            # both players reset their ranks
            Ranks = rand(1:2, 2)
        end
        # updating the confidence bounds
        U1 = S1 ./ N1 + sqrt.(log(t) ./ (2 * N1))
        U2 = S2 ./ N2 + sqrt.(log(t) ./ (2 * N2))
        print("Confidence bounds for player 1: $(U1)\n")
        print("Confidence bounds for player 2: $(U2)\n")
        print("S1=$(S1) and N1=$(N1)\n")
        print("S2=$(S2) and N2=$(N2)\n")
        print("The ranks are $(Ranks)\n")
    end
    print("Total number of collision: $(T - NoCollision) out of $(T)\n")
end


function IterateHybridUCB(Table)
    # Hybrid UCB, no display
    (K, T) = size(Table)
    # uses ranks on top of censored indices
    S1 = zeros(Int, 2)
    N1 = zeros(Int, 2)
    U1 = Inf * ones(2)
    S2 = zeros(Int, 2)
    N2 = zeros(Int, 2)
    U2 = Inf * ones(2)
    Ranks = rand(1:2, 2)
    NoCollision = 0

    # initialization
    I1 = rand(1:2)
    I2 = rand(1:2)
    N1[I1] += 1
    N2[I2] += 1
    if (I1 != I2)
        NoCollision += 1
        S1[I1] += Table[I1, 1]
        S2[I2] += Table[I2, 1]
    end
    I1 = 3 - I1
    I2 = 3 - I2
    N1[I1] += 1
    N2[I2] += 1
    if (I1 != I2)
        NoCollision += 1
        S1[I1] += Table[I1, 2]
        S2[I2] += Table[I2, 2]
    end
    U1 = S1 ./ N1 + sqrt.(log(2) ./ (2 * N1))
    U2 = S2 ./ N2 + sqrt.(log(2) ./ (2 * N2))
    for t = 3:T
        I1 = randmax(U1, Ranks[1])
        I2 = randmax(U2, Ranks[2])
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
            NoCollision += 1
            S1[I1] += Table[I1, T]
            S2[I2] += Table[I2, T]
        else
            # both players reset their ranks
            Ranks = rand(1:2, 2)
        end
        # updating the confidence bounds
        U1 = S1 ./ N1 + sqrt.(log(t) ./ (2 * N1))
        U2 = S2 ./ N2 + sqrt.(log(t) ./ (2 * N2))
    end
    return (T - NoCollision)
end


function RunHybridKLUCB(Table)
    # Hybrid KLUCB, display
    (K, T) = size(Table)
    S1 = zeros(Int, 2)
    N1 = zeros(Int, 2)
    U1 = Inf * ones(2)
    S2 = zeros(Int, 2)
    N2 = zeros(Int, 2)
    U2 = Inf * ones(2)
    Ranks = rand(1:2, 2)
    NoCollision = 0

    # initialization
    print("\n\nSTART \n\n")

    for t = 1:T
        I1 = randmax(U1, Ranks[1])
        I2 = randmax(U2, Ranks[2])
        print("Player 1 chooses $(I1)\n")
        print("Player 2 chooses $(I2)\n\n")
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
            NoCollision += 1
            S1[I1] += Table[I1, t]
            S2[I2] += Table[I2, t]
        else
            print("Collision!\n\n")
            Ranks = rand(1:2, 2)
        end
        # updating the confidence bounds
        for k in 1:2
            if (N1[k] > 0)
                U1[k] = klIC(S1[k] / N1[k], log(t) / N1[k])
            end
        end
        for k in 1:2
            if (N2[k] > 0)
                U2[k] = klIC(S2[k] / N2[k], log(t) / N2[k])
            end
        end
        print("Confidence bounds for player 1: $(U1)\n")
        print("Confidence bounds for player 2: $(U2)\n")
        print("S1=$(S1) and N1=$(N1)\n")
        print("S2=$(S2) and N2=$(N2)\n")
    end
    print("Total number of collision: $(T - NoCollision) out of $(T)\n")
end


function IterateHybridKLUCB(Table)
    # Hybrid KLUCB, no display
    (K, T) = size(Table)
    S1 = zeros(Int, 2)
    N1 = zeros(Int, 2)
    U1 = Inf * ones(2)
    S2 = zeros(Int, 2)
    N2 = zeros(Int, 2)
    U2 = Inf * ones(2)
    Ranks = rand(1:2, 2)
    NoCollision = 0

    # initialization

    for t = 1:T
        I1 = randmax(U1, Ranks[1])
        I2 = randmax(U2, Ranks[2])
        N1[I1] += 1
        N2[I2] += 1
        if (I1 != I2)
        NoCollision += 1
        S1[I1] += Table[I1, t]
        S2[I2] += Table[I2, t]
        else
        Ranks = rand(1:2, 2)
        end
        # updating the confidence bounds
        for k in 1:2
            if (N1[k] > 0)
                U1[k] = klIC(S1[k] / N1[k], log(t) / N1[k])
            end
        end
        for k in 1:2
            if (N2[k] > 0)
                U2[k] = klIC(S2[k] / N2[k], log(t) / N2[k])
            end
        end
    end
    return (T - NoCollision)
end


function Average(algo, mu, T,Nsimu)
    # returns the mean number of collisions of algo running on mu up to horizon T, average over Nsimu runs
    # apply it with the Iterate versions, to avoid too many displays
    meanCollision = 0
    for t in 1:Nsimu
        Table = DrawTable(mu, T)
        meanCollision += algo(Table)
    end
    return (meanCollision / Nsimu)
end


mu = [0.8 0.5 0.2]
T = 100
Table = DrawTable(mu, T)

RunSelfishKLUCB(Table)
# RunSelfishUCB(Table)
# RunHybridKLUCB(Table)

# Average(IterateSelfishKLUCB, mu, T,100)
# Average(IterateHybridKLUCB, mu, T,100)


## a code to find a problematic configuration

Tab = Table

nb = 1000

Coll = zeros(nb)

for k in 1:nb
    Table = DrawTable(mu, 100)
    Coll[k] = IterateSelfishKLUCB(Table)
    if Coll[k] > 50
        Tab = Table
    end
end







