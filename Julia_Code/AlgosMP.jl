# Algorithms in a MAB model in which arm a is a Bernoulli distribution with mean mu[a]

using Distributions

function kl(p, q)
    if (p <= 0) p = eps() end
    if (q >= 1) q = 1 - eps() end
    p * log(p / q) + (1 - p) * log((1 - p) / (1 - q));
end

function klIC(p, d)
    # attention ne gere pas la cas vectoriel comme dans MATLAB
    lM = p;
    uM = min(1, p + sqrt(d / 2));
    for j = 1:16
        qM = (uM + lM) / 2;
        if kl(p, qM) > d
            uM= qM;
        else
            lM = qM;
        end
    end
    qM = uM;
end

function klIClow(p, level)
    # KL lower confidence bound:
    # return lM<p such that d(p, lM) = level
    lM = max(min(1, p - sqrt(level / 2)), 0)
    uM = p
    for j = 1:16
        qM = (uM + lM) / 2;
        if kl(p, qM) > level
            lM= qM;
        else
            uM = qM;
        end
    end
    return(lM)
end

function randmax(vector, rank = 1)
   vector = vec(vector)
   Sorted = sort(vector, rev = true)
   m = Sorted[rank]
   Ind = find(vector .== m)
   index = Ind[floor(Int, length(Ind) * rand()) + 1]
   return (index)
end

function DrawTable(mu, T)
    # WARNING: mu should be ordered in a decreasing way, otherwise it is re - shuffled!
    mu = sort(vec(mu), rev = true)
    K = length(mu)
    Table = zeros(K, T)
    for k = 1:K
        ind = find(rand(T) .< mu[k] * ones(T))
        Table[k, ind] = 1
    end
    return(Table)
end


function ComputeUCB(S, R,N, C,t, feedback, CItype)
    # feedback: "sensing" for vanilla UCB indices, "reward" for reward based indices, "intermediate" for the Ubar indices
    K = length(S)
    I = ones(K)
    Mu = zeros(K)
    # mu depends on the feedback
    for k in 1:K
        if (N[k]>0)
            if feedback == "reward"
                Mu[k] = R[k] / N[k]
            elseif feedback == "intermediate"
                Mu[k] = (1 - C[k] / N[k]) * S[k] / N[k]
            else
                Mu[k] = S[k] / N[k]
            end
        end
    end
    # the UCB depends on CItype
    for k in 1:K
        if (N[k]>0)
            if CItype == "UCB1"
                I[k] = Mu[k] + sqrt(log(t) / (2 * N[k]))
            else
                I[k] = klIC(Mu[k], log(t) / N[k])
            end
        end
    end
    return I
end


## RhoRand [Anandkumar et al. 2011]


function RhoRand(M, Table, Display="Off", feedback="sensing", CItype="KLUCB")
    # uses ranks that are randomly re - allocated after a collision
    K, T = size(Table);
    # quantities ouputed by the algorithm
    Rewards = zeros(M, T);
    ChosenArms = zeros(M, T);
    Collisions = zeros(M, T);
    GoodEvent = zeros(M, T); # does everybody have the true ranking in empirical top - m ?
    GoodEvent2 = zeros(M, T); # is the subset top - m well estimated?
    # local selection storage
    Current = zeros(1, M);
    # current sensing information storage
    S = zeros(M, K); # sensing
    R = zeros(M, K); # reward
    N = zeros(M, K); # draws
    C = zeros(M, K); # collisions
    # set internal ranks for each player at random
    Rank = rand(1:M, M);
    # iteration
    for t = 1:T
        Indices = zeros(M, K)
        # each player selects one arm to draw and performs sensing
        for j = 1:M
            I = ComputeUCB(S[j, :], R[j, :], N[j, :], C[j, :], t,feedback, CItype)
            Indices[j, :] = vec(I)
            # Good event 1 ?
            Sorted = sort(I, rev = true)
            mValue = Sorted[M]
            if (Sorted[1:M] == I[1:M])
                GoodEvent[j, t] = 1
            end
            # Good event 2 ?
            CorrectTop = 1
            for l = 1:M
                if (I[l] < mValue)
                    CorrectTop = 0
                end
            end
            GoodEvent2[j, t] = CorrectTop
            # arm selection
            Value = Sorted[Rank[j]]
            Ind = find(I .== Value);
            chosen = Ind[floor(Int, length(Ind) * rand()) + 1];
            sensing = Table[chosen, t];
            Current[j] = chosen;
            # update the sensing information
            Rewards[j, t]+= sensing;
            ChosenArms[j, t] += chosen;
            S[j, chosen]+= sensing;
            N[j, chosen]+= 1;
        end
        if (Display == "On")
            print("$(Indices) \n")
            print("the current arms are \n $(Current) \n")
        end
        # compute the collision indicator and final reward
        for j = 1:M
            if (length(find(Current .== Current[j]))>1)
                # a collision occurs for player j
                Collisions[j, t] = 1
                C[j, Int(Current[j])] += 1
                Rewards[j, t] = 0
                # player j selects a new rank at random
                Rank[j] = rand(1:M)
            end
            R[j, Int(Current[j])] += Rewards[j, t]
        end
        if (Display == "On")
            print("the ranks are $(Rank) \n\n")
        end
    end
    return(Rewards, N,C, Collisions, ChosenArms, GoodEvent, GoodEvent2)
    end


    ## CUCBRand


    function CUCBRand(M, Table, Display = "Off", feedback = "sensing", CItype = "KLUCB")
    # no ranks, rather target a random arm in the top - m UCB and stay on it until a collision occurs
    K, T = size(Table)
    # quantities ouputed by the algorithm
    Rewards = zeros(Int, M,T);
    ChosenArms = zeros(Int, M,T);
    Collisions = zeros(Int, M,T);
    GoodEvent = zeros(M, T);
    GoodEvent2 = zeros(M, T);
    # local selection storage
    Leave = trues(1, M); # should I stick to the last played arm?
    # current sensing information storage
    S = zeros(M, K);
    R = zeros(M, K);
    N = zeros(M, K);
    C = zeros(M, K);
    # pick a current arm at random for each player
    Current = rand(1:K, M)
    # iteration
    for t = 1:T
    # each player computes the m - best and choose an arm
    Indices = zeros(M, K)
    for j = 1:M
        I = ComputeUCB(S[j, :], R[j, :], N[j, :], C[j, :], t,feedback, CItype)
        Indices[j, :] = vec(I)
        # Good event 1 ?
        Sorted = sort(I, rev = true)
        if (Sorted[1:M] == I[1:M])
            GoodEvent[j, t] = 1
        end
        # Good event 2 ?
        mValue = Sorted[M]
        CorrectTop = 1
        for l = 1:M
            if (I[l] < mValue)
                CorrectTop = 0
            end
        end
        GoodEvent2[j, t] = CorrectTop
        # arm selection
        if (I[Current[j]] < mValue)||(Leave[j])
            # if a collision happened before, or the current arm doesn't belong to the "top - m" UCB set anymore, choose a new random arm in it
            newInd = rand(1:M)
            newVal = Sorted[newInd]
            Ind = find(I .== newVal);
            Current[j] = Ind[floor(Int, length(Ind) * rand()) + 1];
        end
        # perform sensing of the current arm
            chosen = Current[j]
            sensing = Table[chosen, t];
            Rewards[j, t]+= sensing;
            ChosenArms[j, t] += chosen;
            S[j, chosen]+= sensing;
            N[j, chosen] += 1;
        end
        if (Display == "On")
            print("$(Indices)\n")
            print("the current arms are \n $(Current) \n")
        end
        # compute collisions and rewards and update the "Leave" variable
        for j = 1:M
            if (length(find(Current .== Current[j]))>1)
                # a collision occurs for player j
                Leave[j] = true
                Collisions[j, t] = 1
            C[j, Int(Current[j])] += 1
                Rewards[j, t] = 0
            else
                Leave[j] = false
            end
            R[j, Int(Current[j])] = Rewards[j, t]
            end
            if (Display == "On")
            print("Should we leave ? \n")
            print("$(Leave) \n")
        end
    end
    return(Rewards, N,C, Collisions, ChosenArms, GoodEvent, GoodEvent2)
end


## CUCBMC


@everywhere function CUCBMC(M, Table, Display = "Off", feedback = "sensing", CItype = "KLUCB")
    # no ranks, rather target a random arm in the top - m UCB and stay on it once it was free of collision (Musical Chair approach) as long as it remains in top m
    K, T = size(Table)
    # quantities ouputed by the algorithm
    Rewards = zeros(M, T);
    ChosenArms = zeros(M, T);
    Collisions = zeros(M, T);
    GoodEvent = zeros(M, T);
    GoodEvent2 = zeros(M, T);
    # local selection storage
    Leave = trues(1, M); # should I stick to the last played arm?
    # current sensing information storage
    S = zeros(M, K);
    R = zeros(M, K);
    N = zeros(M, K);
    C = zeros(M, K);
    # pick a current arm at random for each player
    Current = rand(1:K, M)
    # iteration
    for t = 1:T
        Indices = zeros(M, K)
        # each player computes the m - best and choose an arm
        for j = 1:M
            I = ComputeUCB(S[j, :], R[j, :], N[j, :], C[j, :], t,feedback, CItype)
            Indices[j, :] = vec(I)
            # Good event 1 ?
            Sorted = sort(I, rev = true)
            if (Sorted[1:M] == I[1:M])
                GoodEvent[j, t] = 1
            end
            # Good event 2 ?
            mValue = Sorted[M]
            CorrectTop = 1
            for l = 1:M
                if (I[l] < mValue)
                    CorrectTop = 0
                end
            end
            GoodEvent2[j, t] = CorrectTop
            # arm selection
            if (Leave[j])||(I[Current[j]] < mValue)
                # a possible new reason to leave: no longer in m - best !
                Leave[j] = true
                newInd = rand(1:M)
                newVal = Sorted[newInd]
                Ind = find(I .== newVal);
                Current[j] = Ind[floor(Int, length(Ind) * rand()) + 1];
            end
            # perform sensing of the current arm
            chosen = Current[j]
            sensing = Table[chosen, t];
            Rewards[j, t]+= sensing;
            ChosenArms[j, t] += chosen;
            S[j, chosen]+= sensing;
            N[j, chosen]+= 1;
        end
        if (Display == "On")
            print("$(Indices)\n")
            print("the current arms are \n $(Current) \n")
        end
        # compute collisions and rewards and update the "Leave" variable
        for j = 1:M
            if (length(find(Current .== Current[j]))>1)
                # a collision occurs for player j
                Collisions[j, t] = 1;
                C[j, Int(Current[j])] += 1;
                Rewards[j, t] = 0;
            else
                # player j shall stick on this arm
                Leave[j] = false
            end
        end
        if (Display == "On")
            print("Should we leave ? \n")
            print("$(Leave) \n")
        end
    end
    return(Rewards, N,C, Collisions, ChosenArms, GoodEvent, GoodEvent2)
end


## Selfish [Bonnefoi et al 2016]


@everywhere function Selfish(M, Table, Display = "Off", feedback = "reward", CItype = "KLUCB")
    # simply target the best index ! (no expected to work with all types of indices)
    K, T = size(Table)
    # quantities ouputed by the algorithm
    Rewards = zeros(Int, M,T);
    ChosenArms = zeros(Int, M,T);
    Collisions = zeros(Int, M,T);
    GoodEvent = zeros(M, T);
    GoodEvent2 = zeros(M, T);
    # current sensing information storage
    S = zeros(M, K);
    R = zeros(M, K);
    N = zeros(M, K);
    C = zeros(M, K);
    # pick a current arm at random for each player
    Current = rand(1:K, M)
    # iteration
    for t = 1:T
        # each player computes the arm with highest UCB
        Indices = zeros(M, K)
        for j = 1:M
            I = ComputeUCB(S[j, :], R[j, :], N[j, :], C[j, :], t,feedback, CItype)
            Indices[j, :] = vec(I)
            chosen = randmax(I)
            Current[j] = chosen
            # Good event 1 ?
            Sorted = sort(I, rev = true)
            if (Sorted[1:M] == I[1:M])
                GoodEvent[j, t] = 1
            end
            # Good event 2 ?
            mValue = Sorted[M]
            CorrectTop = 1
            for l = 1:M
                if (I[l] < mValue)
                    CorrectTop = 0
                end
            end
            GoodEvent2[j, t] = CorrectTop
            # perform sensing of the current arm
            chosen = Current[j]
            sensing = Table[chosen, t];
            ChosenArms[j, t] += chosen;
            S[j, chosen]+= sensing;
            N[j, chosen]+= 1;
        end
        if (Display == "On")
            print("$(Indices)\n")
            print("the current arms are \n $(Current) \n")
        end
        # check if there is a collision and update the tables accordingly
        for j = 1:M
            if (length(find(Current .== Current[j]))>1)
                # a collision occurs for player j
                Collisions[j, t] = 1
                C[j, Int(Current[j])] += 1
                Rewards[j, t] = 0
            else
                # sensing is performed and information updated
                sensing = Table[Int(Current[j]), t]
                R[j, Int(Current[j])] += sensing
                Rewards[j, t] = sensing
            end
        end
    end
    return(Rewards, N,C, Collisions, ChosenArms, GoodEvent, GoodEvent2)
end



## ETC - MC (A m - best arm identification followed by a MC phase)


function ETCMC(M, Table, Display = "Off")
    # good event not modified and not accurate
    K, T = size(Table)
    # quantities ouputed by the algorithm
    Rewards = zeros(M, T);
    ChosenArms = zeros(M, T);
    Collisions = zeros(M, T);
    GoodEvent = zeros(M, T);
    GoodEvent2 = zeros(M, T);
    # local selection storage
    Leave = trues(1, M); # should I leave the last played arm?
    TopM = zeros(Int, M,M); # storing estimated top - m for each player (useful after exploration phase)
    Explore = trues(1, M); # should each player still explore?
    # current sensing information storage
    S = zeros(M, K);
    N = zeros(M, K);
    C = zeros(M, K);
    # pick a current arm at random for each player
    Current = rand(1:K, M)
    IndicesUp = zeros(M, K)
    IndicesLow = zeros(M, K)
    # iteration
    for t = 1:T
        for j = 1:M
            if (Explore[j])
                # player j performs KL - UCB
                UCB = ones(K)
                LCB = zeros(K)
                for k = 1:K
                    if (N[j, k]>0)
                        UCB[k] = klIC(S[j, k] / N[j, k], log((1 + log(t)) * T) / N[j, k])
                        LCB[k] = klIClow(S[j, k] / N[j, k], log((1 + log(t)) * T) / N[j, k])
                    end
                end
                IndicesUp[j, :] = UCB
                IndicesLow[j, :] = LCB
                # KL - LUCB algorithm
                indices = sortperm(vec(S[j, :] ./ N[j, :]), rev = true)
                TopArms = indices[1:M]
                BottomArms = indices[(M + 1):K]
                minLUCB = minimum([LCB[k] for k in TopArms])
                maxUCB = maximum([UCB[k] for k in BottomArms])
                if (minLUCB>maxUCB)
                    # m - best arm have been identified
                    Explore[j] = false
                    TopM[j, :] = TopArms'
                    Current[j] = TopArms[rand(1:M)]
                else
                    best = randmax( - LCB[TopArms])
                    best = TopArms[best]
                    challenger = randmax(UCB[BottomArms])
                    challenger = BottomArms[challenger]
                    # select the least drawn of the estimated best and its challenger
                    Current[j] = (N[j, best]<N[j, challenger])?best:challenger
                end
            else
                # perform musical chair
                if (Leave[j])
                    # pick at random one of the m - best
                    Current[j] = TopM[j, rand(1:M)]
                end
            end
            # draw the current arm and perform sensing
            chosen = Current[j]
            sensing = Table[chosen, t];
            Rewards[j, t]+= sensing;
            ChosenArms[j, t] += chosen;
            S[j, chosen]+= sensing;
            N[j, chosen]+= 1;
        end
        if (Display == "On")
            print("UCB is $(IndicesUp)\n")
            print("LCB is $(IndicesLow)\n")
            print("the current arms are \n $(Current) \n")
        end
        # compute collisions and rewards and update the "Leave" variable
        for j = 1:M
            if (length(find(Current .== Current[j]))>1)
                # a collision occurs for player j
                Collisions[j, t] = 1;
                C[j, Int(Current[j])] += 1;
                Rewards[j, t] = 0;
            elseif (Explore[j] == false)
                # no collision and m - best have been identified: stick on the arm
                Leave[j] = false
            end
        end
        if (Display == "On")
            print("Should we leave ? \n")
            print("$(Leave) \n")
        end
    end
    return(Rewards, N,C, Collisions, ChosenArms, GoodEvent, GoodEvent2)
end


